package main

import "net/http"
import "fmt"

func hello(w http.ResponseWriter, req *http.Request) {
    fmt.Fprintf(w, "hello test\n")
}

func main() {
    http.HandleFunc("/", hello)
    http.ListenAndServe(":8090", nil)
}